using FilmsCatalog.Domain.Aggregates.FilmEntities;
using FilmsCatalog.Domain.Aggregates.ImageEntities;
using FilmsCatalog.Domain.Models.Dtos.FilmDtos;
using FilmsCatalog.Domain.Models.Dtos.ImageDtos;

namespace FilmsCatalog.Domain.Extensions
{
    public static class ObjectConverter
    {
        public static ImageForUploadDto ToImageForUploadDto(this CreateImageDto createImageDto)
        {
            return new ImageForUploadDto()
            {
                File = createImageDto.Image.OpenReadStream(),
                ContentType = createImageDto.Image.ContentType,
                FileName = createImageDto.Image.FileName,
                Path = createImageDto.Path
            };
        }

        public static ImageDto ToImageDto(this ImageEntity imageEntity)
        {
            return new ImageDto()
            {
                Id = imageEntity.Id,
                FileName = imageEntity.FileName,
                Path = imageEntity.Path
            };
        }

        public static FilmDto ToFilmDto(this Film film, string posterPath)
        {
            return new FilmDto()
            {
                Id = film.Id,
                Name = film.Name,
                Description = film.Description,
                Producer = film.Producer,
                ReleaseYear = film.ReleaseYear,
                PosterId = film.PosterId,
                PosterPath = posterPath
            };
        }
    }
}