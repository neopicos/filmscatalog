using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using FilmsCatalog.Domain.Models.Dtos.ImageDtos;
using Microsoft.AspNetCore.Http;

namespace FilmsCatalog.Domain.Models.Dtos.FilmDtos
{
    /// <summary>
    /// DTO-представление фильма
    /// </summary>
    public class  FilmDto
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// DTO-представление изображения постера
        /// </summary>
        [DisplayName("Файл с изображением постера")]
        public IFormFile PosterImage { get; set; }

        /// <summary>
        /// Идентификатор пользователя, опубликовавшего информацию о данном фильме
        /// </summary>
        public Guid PublisherUserId { get; set; }

        /// <summary>
        /// Идентификатор изображение постера
        /// </summary>
        public Guid PosterId { get; set; }

        /// <summary>
        /// Путь к постеру
        /// </summary>
        public string PosterPath { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        [DisplayName("Название")]
        public string Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        [DisplayName("Описание")]
        public string Description { get; set; }

        /// <summary>
        /// Год выпуска
        /// </summary>
        [DisplayName("Год выпуска")]
        [RegularExpression("^[1-9][0-9]{3}$", ErrorMessage = "Неверный формат года")]
        public int ReleaseYear { get; set; }

        /// <summary>
        /// Режиссер
        /// </summary>
        [DisplayName("Продюсер")]
        public string Producer { get; set; }
    }
}