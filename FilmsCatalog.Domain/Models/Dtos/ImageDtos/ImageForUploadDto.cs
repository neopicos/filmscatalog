using System.IO;

namespace FilmsCatalog.Domain.Models.Dtos.ImageDtos
{
    public class ImageForUploadDto
    {
        public Stream File { get; set; }

        public string FileName { get; set; }

        public string ContentType { get; set; }

        public string Path { get; set; }
    }
}