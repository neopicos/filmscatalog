using Microsoft.AspNetCore.Http;

namespace FilmsCatalog.Domain.Models.Dtos.ImageDtos
{
    public class CreateImageDto
    {
        public IFormFile Image { get; set; }

        public string Path { get; set; }
    }
}