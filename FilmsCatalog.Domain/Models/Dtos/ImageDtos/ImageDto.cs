using System;

namespace FilmsCatalog.Domain.Models.Dtos.ImageDtos
{
    public class ImageDto
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Путь
        /// </summary>
        public string Path { get; set; }
    }
}