using System;
using System.Threading.Tasks;
using FilmsCatalog.Domain.Models.Dtos.ImageDtos;

namespace FilmsCatalog.Domain.Services.Interfaces
{
    public interface IImageService
    {
        Task<ImageDto> CreateImageAsync(CreateImageDto createImageDto);

        Task<ImageDto> GetImageByIdAsync(Guid id);

        Task DeleteImageAsync(Guid id);
    }
}