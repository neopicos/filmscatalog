using System.Threading.Tasks;
using FilmsCatalog.Domain.Models.Dtos.ImageDtos;

namespace FilmsCatalog.Domain.Services.Interfaces
{
    public interface IImageUploadService
    {
        Task<string> UploadImageAsync(ImageForUploadDto image);
    }
}