using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FilmsCatalog.Domain.Models.Dtos.FilmDtos;

namespace FilmsCatalog.Domain.Services.Interfaces
{
    public interface IFilmService
    {
        Task<List<FilmDto>> GetFilmListAsync();

        Task<FilmDto> GetFilmByIdAsync(Guid id);

        Task<FilmDto> CreateFilmAsync(FilmDto filmDto);

        Task DeleteFilmAsync(Guid id);

        Task<FilmDto> UpdateFilmAsync(FilmDto filmDto);
    }
}