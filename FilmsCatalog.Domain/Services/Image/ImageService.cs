using System;
using System.Threading.Tasks;

using FilmsCatalog.Domain.Aggregates.ImageEntities;
using FilmsCatalog.Domain.Extensions;
using FilmsCatalog.Domain.Interfaces.Repositories;
using FilmsCatalog.Domain.Models.Dtos.ImageDtos;
using FilmsCatalog.Domain.Providers;
using FilmsCatalog.Domain.Services.Interfaces;

namespace FilmsCatalog.Domain.Services
{
    public class ImageService : IImageService
    {
        private readonly ICurrentDateProvider _currentDateProvider;
        private readonly IImageEntityRepository _imageEntityRepository;
        private readonly IImageUploadService _imageUploadService;

        public ImageService(ICurrentDateProvider currentDateProvider,
            IImageEntityRepository imageEntityRepository,
            IImageUploadService imageUploadService)
        {
            _currentDateProvider = currentDateProvider ??
                                        throw new ArgumentNullException(nameof(currentDateProvider));
            
            _imageEntityRepository = imageEntityRepository ??
                                        throw new ArgumentNullException(nameof(imageEntityRepository));
            
            _imageUploadService = imageUploadService ??
                                  throw new ArgumentNullException(nameof(imageUploadService));
        }
        
        /// <summary>
        /// Добавление нового изображения
        /// </summary>
        /// <param name="createImageDto">DTO-изображения на создание</param>
        /// <returns>DTO-представление изображения</returns>
        public async Task<ImageDto> CreateImageAsync(CreateImageDto createImageDto)
        {
            var image = new ImageEntity(createImageDto,
                _currentDateProvider.GetDate());

            // Добавление нового изображения
            await _imageEntityRepository.CreateAsync(image);

            var imageForUploadDto = createImageDto.ToImageForUploadDto();
            await _imageUploadService.UploadImageAsync(imageForUploadDto);

            // Коммит текущей транзакции
            await _imageEntityRepository.SaveChangeAsync();
            
            // Формирование ответа
            var imageResponse = image.ToImageDto();

            return imageResponse;
        }

        /// <summary>
        /// Получение изображения по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <returns>DTO-представление изображения</returns>
        public async Task<ImageDto> GetImageByIdAsync(Guid id)
        {
            var image = await _imageEntityRepository.GetByIdAsync(id);
            if (image == null)
            {
                // TODO: необходимо генерировать костомное исключение (прим. ImageWasNotFoundException)
                throw new Exception();
            }
            
            // Формирование ответа
            var imageResponse = image.ToImageDto();

            return imageResponse;
        }

        /// <summary>
        /// Удаление существующего изображения по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        public async Task DeleteImageAsync(Guid id)
        {
            var image = await _imageEntityRepository.GetByIdAsync(id);
            if (image == null)
            {
                // TODO: необходимо генерировать костомное исключение (прим. ImageWasNotFoundException)
                throw new Exception();
            }

            await _imageEntityRepository.DeleteAsync(image);

            await _imageEntityRepository.SaveChangeAsync();
        }
    }
}