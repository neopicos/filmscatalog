using System.IO;
using System.Threading.Tasks;
using FilmsCatalog.Domain.Models.Dtos.ImageDtos;
using FilmsCatalog.Domain.Services.Interfaces;

namespace FilmsCatalog.Domain.Services
{
    public class ImageUploadToFileSystemService : IImageUploadService
    {
        /// <summary>
        /// Загрузка файла изображения в файловую систему
        /// </summary>
        /// <param name="image">DTO-представление изображения</param>
        /// <returns>Путь к файлу в файловой системе</returns>>
        public async Task<string> UploadImageAsync(ImageForUploadDto image)
        {
            var path = image.Path;
            await using var fileStream = new FileStream(path,
                FileMode.Create);

            image.File.CopyToAsync(fileStream);
            
            return path;
        }
    }
}