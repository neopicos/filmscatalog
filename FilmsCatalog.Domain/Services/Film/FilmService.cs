using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FilmsCatalog.Domain.Aggregates.FilmEntities;
using FilmsCatalog.Domain.Extensions;
using FilmsCatalog.Domain.Interfaces.Repositories;
using FilmsCatalog.Domain.Models.Dtos.FilmDtos;
using FilmsCatalog.Domain.Models.Dtos.ImageDtos;
using FilmsCatalog.Domain.Providers;
using FilmsCatalog.Domain.Services.Interfaces;

namespace FilmsCatalog.Domain.Services
{
    public class FilmService : IFilmService
    {
        private readonly IFilmRepository _filmRepository;
        private readonly IImageService _imageService;
        private readonly ICurrentDateProvider _currentDateProvider;

        public FilmService(IFilmRepository filmRepository,
            ICurrentDateProvider currentDateProvider,
            IImageService imageService)
        {
            _filmRepository = filmRepository ??
                              throw new ArgumentNullException(nameof(filmRepository));
            
            _currentDateProvider = currentDateProvider ??
                                   throw new ArgumentNullException(nameof(currentDateProvider));
            
            _imageService = imageService ??
                            throw new ArgumentNullException(nameof(imageService));
        }
        
        /// <summary>
        /// Получение списка всех фильмов
        /// </summary>
        /// <returns>Список DTO-представлений фильмов</returns>
        public async Task<List<FilmDto>> GetFilmListAsync()
        {
            var films = await _filmRepository.GetListAsync();

            // Формирование ответа
            var response = new List<FilmDto>();
            foreach (var film in films)
            {
                var poster = await _imageService.GetImageByIdAsync(film.PosterId);
                response.Add(film.ToFilmDto(poster.Path));
            }

            return response;
        }

        /// <summary>
        /// Получение фильма по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <returns>DTO-представление фильма</returns>
        public async Task<FilmDto> GetFilmByIdAsync(Guid id)
        {
            // Получение фильма
            var film = await _filmRepository.GetByIdAsync(id);
            if (film == null)
            {
                // TODO: необходимо генерировать костомное исключение (прим. FilmWasNotFoundException)
                throw new Exception();
            }
            
            // Получение ппостера к фильму
            var poster = await _imageService.GetImageByIdAsync(film.PosterId);
            
            // Формирование ответа
            var filmDto = film.ToFilmDto(poster.Path);

            return filmDto;
        }

        /// <summary>
        /// Добавление нового фильма
        /// </summary>
        /// <param name="filmDto">DTO-представление фильма</param>
        /// <returns>DTO-представление созданного фильма</returns>
        public async Task<FilmDto> CreateFilmAsync(FilmDto filmDto)
        {
            // Добавление нового постера
            var filmPosterImage = filmDto.PosterImage;
            var imageInfo = await _imageService.CreateImageAsync(new CreateImageDto()
            {
                Image = filmPosterImage,
                Path = filmDto.PosterPath
            });
            
            // Добавление нового фильма
            var film = new Film(filmDto, _currentDateProvider.GetDate(), imageInfo.Id);
            await _filmRepository.CreateAsync(film);
            
            // Коммит текущей транзакции
            await _filmRepository.SaveChangeAsync();

            return filmDto;
        }

        /// <summary>
        /// Удаление существующего фильма
        /// </summary>
        /// <param name="id">Идентификатор</param>
        public async Task DeleteFilmAsync(Guid id)
        {
            // Получение фильма
            var film = await _filmRepository.GetByIdAsync(id);
            if (film == null)
            {
                // TODO: необходимо генерировать костомное исключение (прим. FilmWasNotFoundException)
                throw new Exception();
            }
            
            // Удаление постера
            await _imageService.DeleteImageAsync(film.PosterId);
            
            // Удаление фильма
            await _filmRepository.DeleteAsync(film);
        }

        /// <summary>
        /// Изменение данных существующего фильма
        /// </summary>
        /// <param name="filmDto">DTO-представление фильма</param>
        /// <returns>DTO-представление измененного фильма</returns>
        public async Task<FilmDto> UpdateFilmAsync(FilmDto filmDto)
        {
            var film = await _filmRepository.GetByIdAsync(filmDto.Id);
            if (film == null)
            {
                // TODO: необходимо генерировать костомное исключение (прим. FilmWasNotFoundException)
                throw new Exception();
            }
            
            // Перезагрузка постера
            await _imageService.DeleteImageAsync(film.PosterId);
            var filmPosterImage = filmDto.PosterImage;
            var imageInfo = await _imageService.CreateImageAsync(new CreateImageDto()
            {
                Image = filmPosterImage,
                Path = filmDto.PosterPath
            });

            return null;
        }
    }
}