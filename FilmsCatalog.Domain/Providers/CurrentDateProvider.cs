using System;

namespace FilmsCatalog.Domain.Providers
{
    public class CurrentDateProvider : ICurrentDateProvider
    {
        public DateTime GetDate() => DateTime.Now;
    }
}