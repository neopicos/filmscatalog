using System;

namespace FilmsCatalog.Domain.Providers
{
    public interface ICurrentDateProvider
    {
        DateTime GetDate();
    }
}