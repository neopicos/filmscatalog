using System;

namespace FilmsCatalog.Domain.Interfaces
{
    public interface IDomainEntity
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// Дата создания
        /// </summary>
        DateTime DatePost { get; }

        /// <summary>
        /// Дата последнего изменения
        /// </summary>
        DateTime DateLastUpdate { get; }
    }
}