using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FilmsCatalog.Domain.Aggregates;

namespace FilmsCatalog.Domain.Interfaces.Repositories
{
    public interface IRepository<T>
        where T : DomainEntity
    {
        Task<List<T>> GetListAsync();
        
        Task<T> GetByIdAsync(Guid id);

        Task CreateAsync(T item);

        Task UpdateAsync(T item);

        Task DeleteAsync(T item);

        Task SaveChangeAsync();
    }
}