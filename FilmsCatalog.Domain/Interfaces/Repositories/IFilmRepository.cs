using FilmsCatalog.Domain.Aggregates.FilmEntities;

namespace FilmsCatalog.Domain.Interfaces.Repositories
{
    public interface IFilmRepository : IRepository<Film>
    {
        
    }
}