using FilmsCatalog.Domain.Aggregates.ImageEntities;

namespace FilmsCatalog.Domain.Interfaces.Repositories
{
    public interface IImageEntityRepository : IRepository<ImageEntity>
    {
        
    }
}