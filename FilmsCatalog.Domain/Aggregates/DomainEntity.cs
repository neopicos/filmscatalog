using System;
using FilmsCatalog.Domain.Interfaces;

namespace FilmsCatalog.Domain.Aggregates
{
    public class DomainEntity : IDomainEntity
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; protected set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime DatePost { get; protected set; }
        
        /// <summary>
        /// Дата последнего изменения
        /// </summary>
        public DateTime DateLastUpdate { get; protected set; }
        
        /// <summary>
        /// Создание нового идентификатора
        /// </summary>
        /// <returns></returns>
        protected Guid CreateNewId()
            => Guid.NewGuid();
    }
}