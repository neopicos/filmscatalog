using System;
using FilmsCatalog.Domain.Aggregates.UserEntities;
using FilmsCatalog.Domain.Aggregates.ImageEntities;
using FilmsCatalog.Domain.Models.Dtos.FilmDtos;

namespace FilmsCatalog.Domain.Aggregates.FilmEntities
{
    /// <summary>
    /// Фильм
    /// </summary>
    public class Film : DomainEntity
    {
        /// <summary>
        /// Пользователь, опубликовавший информацию о данном фильме
        /// </summary>
        public Guid PublisherUserId { get; private set; }

        /// <summary>
        /// Изображение постера
        /// </summary>
        public Guid PosterId { get; private set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Год выпуска
        /// </summary>
        public int ReleaseYear { get; private set; }

        /// <summary>
        /// Режиссер
        /// </summary>
        public string Producer { get; private set; }

        public Film(FilmDto dtoModel, DateTime currentDate, Guid posterID)
        {
            if (dtoModel == null)
            {
                throw new ArgumentNullException(nameof(dtoModel));
            }

            if (currentDate == default)
            {
                throw new ArgumentException(null, nameof(currentDate));
            }

            if (posterID == default)
            {
                throw new ArgumentException(null, nameof(posterID));
            }

            Id = CreateNewId();
            Name = dtoModel.Name;
            Description = dtoModel.Description;
            Producer = dtoModel.Producer;
            ReleaseYear = dtoModel.ReleaseYear;
            DatePost = currentDate;
            DateLastUpdate = currentDate;
            PosterId = posterID;
        }

        public Film()
        {
            
        }
    }
}