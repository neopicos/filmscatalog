using System;
using FilmsCatalog.Domain.Models.Dtos.ImageDtos;

namespace FilmsCatalog.Domain.Aggregates.ImageEntities
{
    /// <summary>
    /// Изображение
    /// </summary>
    public class ImageEntity : DomainEntity
    {
        /// <summary>
        /// Наименование
        /// </summary>
        public string FileName { get; private set; }

        /// <summary>
        /// Тип
        /// </summary>
        public string ContentType { get; private set; }

        /// <summary>
        /// Размер в байтах
        /// </summary>
        public long SizeByte { get; private set; }

        /// <summary>
        /// Путь в файловой системе
        /// </summary>
        public string Path { get; private set; }

        /// <summary>
        /// Инициализирующий конструктор
        /// </summary>
        /// <param name="validModel">DTO изображения</param>
        /// <param name="currentDate">Текущая дата</param>
        public ImageEntity(CreateImageDto dtoModel, DateTime currentDate)
        {
            if (dtoModel == null)
            {
                throw new ArgumentNullException(nameof(dtoModel));
            }

            if (currentDate == default)
            {
                throw new ArgumentException(null, nameof(currentDate));
            }

            var image = dtoModel.Image;

            // Конструирование объекта
            Id = CreateNewId();
            FileName = image.FileName;
            ContentType = image.ContentType;
            SizeByte = image.Length;
            DatePost = currentDate;
            Path = "/Files/" + FileName;
        }
        
        public ImageEntity()
        {
            
        }
    }
}