using System;
using FilmsCatalog.Domain.Aggregates.FilmEntities;
using FilmsCatalog.Domain.Interfaces.Repositories;

namespace FilmsCatalog.DataAccess.Data.Repositories
{
    public class FilmRepository : Repository<Film>,
        IFilmRepository
    {
        protected ApplicationDbContext _dbContext;
        
        public FilmRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext ?? 
                         throw new ArgumentNullException(nameof(dbContext));
        }
    }
}