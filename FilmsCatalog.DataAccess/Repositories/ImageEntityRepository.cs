using FilmsCatalog.Domain.Aggregates.ImageEntities;
using FilmsCatalog.Domain.Interfaces.Repositories;

namespace FilmsCatalog.DataAccess.Data.Repositories
{
    public class ImageEntityRepository : Repository<ImageEntity>,
        IImageEntityRepository
    {
        protected ApplicationDbContext _dbContext;
        
        public ImageEntityRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }
    }
}