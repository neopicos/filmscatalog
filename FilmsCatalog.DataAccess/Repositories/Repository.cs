using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FilmsCatalog.Domain.Aggregates;
using FilmsCatalog.Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;

namespace FilmsCatalog.DataAccess.Data.Repositories
{
    public class Repository<T> : IRepository<T>
        where T : DomainEntity
    {
        protected ApplicationDbContext _dbContext { get; private set; }

        public Repository(ApplicationDbContext dbContext)
        {
            this._dbContext = dbContext ??
                              throw new ArgumentNullException(nameof(dbContext));
        }

        public async Task<List<T>> GetListAsync()
        {
            return await _dbContext.Set<T>().ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _dbContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task CreateAsync(T item)
        {
            await _dbContext.Set<T>().AddAsync(item);
        }

        public async Task UpdateAsync(T item)
        {
            _dbContext.Set<T>().Update(item);
        }

        public async Task DeleteAsync(T item)
        {
            _dbContext.Set<T>().Remove(item);
        }

        public async Task SaveChangeAsync()
        {
            await _dbContext.SaveChangesAsync();
        }
    }
}