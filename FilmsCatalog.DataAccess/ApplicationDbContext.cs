﻿using FilmsCatalog.Domain.Aggregates.UserEntities;
using FilmsCatalog.Domain.Aggregates.FilmEntities;
using FilmsCatalog.Domain.Aggregates.ImageEntities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FilmsCatalog.DataAccess.Data
{
    public class ApplicationDbContext : IdentityDbContext<User>
    {
        public DbSet<Film> Films { get; set; }

        public DbSet<ImageEntity> FilmPosters { get; set; }
        
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
