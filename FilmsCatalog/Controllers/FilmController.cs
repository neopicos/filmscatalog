using System;
using System.IO;
using System.Threading.Tasks;
using FilmsCatalog.Domain.Models.Dtos.FilmDtos;
using FilmsCatalog.Domain.Services.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace FilmsCatalog.Controllers
{
    public class FilmController : Controller
    {
        private readonly IFilmService _filmService;
        private readonly IWebHostEnvironment _appEnvironment;

        public FilmController(IFilmService filmService, IWebHostEnvironment appEnvironment)
        {
            _filmService = filmService ?? throw new ArgumentNullException(nameof(filmService));
            _appEnvironment = appEnvironment;
        }

        [HttpGet]
        [Route("Film/")]
        public async Task<IActionResult> Index()
        {
            var films  = await _filmService.GetFilmListAsync();
            
            return View(films);
        }
            
        [Route("Film/Create")]
        public IActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Добавление нового фильма
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> CreateFilmAsync(FilmDto filmDto)
        {
            var path = _appEnvironment.WebRootPath + "/" + filmDto.PosterImage.FileName;
            filmDto.PosterPath = path;
            
            await _filmService.CreateFilmAsync(filmDto);

            return Redirect("Film/");
        }
    }
}